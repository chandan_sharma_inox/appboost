package com.app.ibooster;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BoosterReceiver extends BroadcastReceiver{
	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i(Booster.TAG, "broadcast reiceived.");
		if(intent != null){
			if(intent.getAction() == Intent.ACTION_BOOT_COMPLETED){
				Log.i(Booster.TAG, "Device boot recieved");
				String message[] = {Booster.verifyMessage(context,  Utilities.PREF_FIRST_LINE, Utilities.getMessage(context, Utilities.PREF_FIRST_LINE)),
						Booster.verifyMessage(context,  Utilities.PREF_SECOND_LINE, Utilities.getMessage(context, Utilities.PREF_SECOND_LINE)),
						Booster.verifyMessage(context,  Utilities.PREF_APP_ICON, Utilities.getMessage(context, Utilities.PREF_APP_ICON))};
				Booster.startBoost(context, message);
			}
			else{
				byte type = intent.getByteExtra(AppReminder.ALERT_TYPE, (byte)-1);
				Log.i(Booster.TAG, "receiver type : "+type);
				if(type == AppReminder.NOTIFICATION_DELETE){
					String message[] = {Booster.verifyMessage(context,  Utilities.PREF_FIRST_LINE, Utilities.getMessage(context, Utilities.PREF_FIRST_LINE)),
							Booster.verifyMessage(context,  Utilities.PREF_SECOND_LINE, Utilities.getMessage(context, Utilities.PREF_SECOND_LINE)),
							Booster.verifyMessage(context,  Utilities.PREF_APP_ICON, Utilities.getMessage(context, Utilities.PREF_APP_ICON))};
					Booster.startBoost(context, message);
				}
				else
					if(type == AppReminder.NOTIFICATION_CONTENT){
						// posting click to server
						Intent pintent = new Intent(context, BoosterService.class);
						pintent.putExtra(BoosterService.SDK_ACTION, BoosterService.REACH_TYPE_CLICK);
						context.startService(pintent);
						
						// starting user's activity
						startUserActivity(context);
					}
				else{
					String message[] = {Booster.verifyMessage(context,  Utilities.PREF_FIRST_LINE, Utilities.getMessage(context, Utilities.PREF_FIRST_LINE)),
							Booster.verifyMessage(context,  Utilities.PREF_SECOND_LINE, Utilities.getMessage(context, Utilities.PREF_SECOND_LINE)),
							Booster.verifyMessage(context,  Utilities.PREF_APP_ICON, Utilities.getMessage(context, Utilities.PREF_APP_ICON))};
					AppReminder.remind(context, AppReminder.REMINDER_TYPE_NOTIFICATION, message);
				}
			}
		}
	}
	
	private void startUserActivity(Context context){
		Log.i(Booster.TAG, "starting activity");
		String classname = Booster.getRegisterClass(context);
		if(classname != null){
			try {
				Intent startintent = new Intent(context,Class.forName(classname));
				startintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(startintent);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
