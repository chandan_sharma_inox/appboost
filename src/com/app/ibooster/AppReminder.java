package com.app.ibooster;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.util.Log;

import com.example.appbooster.R;

public class AppReminder {
	
	public final static byte REMINDER_TYPE_NOTIFICATION = 1;
	public final static byte REMINDER_TYPE_DIALOG = 2;
	public final static byte REMINDER_TYPE_POPUP = 3;
	
	public final static String ALERT_TYPE = "TYPE";
	
	public final static byte NOTIFICATION_DELETE = 4;
	public final static byte NOTIFICATION_UPDATE = 5;
	public final static byte NOTIFICATION_CONTENT = 6;
	
	// hypo id
	public final static int NOTIFICATION_ID = 1500;
	
	private static String boost_message[];
	
	public static void remind(Context context , byte type, String description[]){
		switch(type){
		case REMINDER_TYPE_NOTIFICATION:
			boost_message = description;
			showNotification(context);
			Intent intent = new Intent(context, BoosterService.class);
			intent.putExtra(BoosterService.SDK_ACTION, BoosterService.SHOW_TYPE_NOTI);
			context.startService(intent);
			break;
		}
	}
	
	private static void showNotification(Context context) {
		Log.i(Booster.TAG, "showing notification with "+boost_message[0]+"::"+boost_message[1]);
		Log.i(Booster.TAG, "showing notification "+boost_message[2]);
		NotificationManager manager = ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE));
		
		int icon = 0;
		try{
			icon = Integer.parseInt(boost_message[2]);
			if(icon < 1){
				icon = context.getApplicationInfo().icon;
			}
		}
		catch(Exception e){
			icon = context.getApplicationInfo().icon;
		}
		
		Notification notification;
		notification = new Notification(icon, boost_message[1], System.currentTimeMillis());
		
		PackageManager pm = context.getPackageManager();
		
		notification.flags = Notification.FLAG_AUTO_CANCEL;
		notification.setLatestEventInfo(context, boost_message[0], boost_message[1], null);
		
		Intent clickintent = new Intent(context, BoosterReceiver.class);
		clickintent.putExtra(ALERT_TYPE, NOTIFICATION_CONTENT);
		clickintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		clickintent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
		clickintent.setAction(Intent.ACTION_VIEW);
		notification.contentIntent = PendingIntent.getBroadcast(context, NOTIFICATION_ID,
										clickintent,
						                PendingIntent.FLAG_CANCEL_CURRENT);
		
		Intent deleteintent = new Intent(context, BoosterReceiver.class);
		deleteintent.putExtra(ALERT_TYPE, NOTIFICATION_DELETE);
		notification.deleteIntent = PendingIntent.getBroadcast(context, NOTIFICATION_ID, deleteintent, PendingIntent.FLAG_CANCEL_CURRENT);
		
		manager.notify(NOTIFICATION_ID, notification);
		sendBigPictureStyleNotification(context);
		sendInboxStyleNotification(context, manager);
		showHtmlNotification(context);
	}
	
	@SuppressLint("NewApi") 
	public static void sendBigPictureStyleNotification(Context context) {
		
		Intent intent = new Intent(
				Intent.ACTION_VIEW,
				Uri.parse("market://details?id=com.pink.clock.widget.theme"));
		PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);
		
		NotificationManager notificationManager = ((NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE));
		Builder builder = new Notification.Builder(context);
		builder.setContentTitle("BP notification")
				.setContentText("BigPicutre notification")
				.setSmallIcon(R.drawable.ic_launcher)
				.addAction(R.drawable.ic_launcher, "show activity", pIntent)
				.addAction(R.drawable.ic_launcher, "Share", null)
				.addAction(R.drawable.ic_launcher, "Share2", null)
				.addAction(R.drawable.ic_launcher, "Share3", null)
				.addAction(R.drawable.ic_launcher, "Share4", null);

		int id = getDrawableByName(context, "mic");
		Notification notification = new Notification.BigPictureStyle(builder)
				.bigPicture(
						BitmapFactory.decodeResource(context.getResources(),
								id)).build();
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notificationManager.notify(5, notification);
	}
	

	// XXX use this code in case of expandable/collapsible inbox style
	// notification.
	@SuppressLint("NewApi") 
	public static void sendInboxStyleNotification(Context context,
			NotificationManager notificationManager) {
		int id = getDrawableByName(context, "xingagcm_11");
		Builder builder = new Notification.Builder(context)
				.setContentTitle("IS Notification")
				.setContentText("Inbox Style notification!!")
				.setSmallIcon(R.drawable.ic_launcher)
				.addAction(id, "show activity", null);

		Notification notification = new Notification.InboxStyle(builder)
				.addLine("First message").addLine("Second message")
				.addLine("Thrid message")
				.setSummaryText("+2 more")
				.setBigContentTitle("BigContent title")
				.setSummaryText("summary text").build();
		// Put the auto cancel notification flag
		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		notificationManager.notify(2, notification);
	}

	// XXX use this code in case of html notification.
	@SuppressLint("NewApi")
	public static void showHtmlNotification(Context context) {
		int id1 = getDrawableByName(context, "xingagcm_12");
		int id2 = getDrawableByName(context, "xingagcm_8");
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				context)
				.addAction(id2, "show html", null);
		mBuilder.setContentText("content text");
		mBuilder.setContentTitle("context title");
		mBuilder.setSubText("content sub text");
		mBuilder.setSmallIcon(id2);
		
		
		NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
		inboxStyle.setBigContentTitle("Title");
		inboxStyle.setSummaryText("summary text");
		inboxStyle.addLine(Html
				.fromHtml("<i>italic</i> <b>bold</b> text works"));
		inboxStyle.addLine(Html
				.fromHtml("<i>italic</i> <b>bold</b> text works"));
		inboxStyle.addLine(Html
				.fromHtml("<i>italic</i> <b>bold</b> text works"));
		mBuilder.setStyle(inboxStyle);
		Log.i(Booster.TAG, "Showing call recorder notification");
		NotificationManager mNotificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify(105, mBuilder.build());
	}

	
	public static int getDrawableByName(Context ctx, String name) {
		return ctx.getResources().getIdentifier(name, "drawable",
				ctx.getPackageName());
	}
	
}
