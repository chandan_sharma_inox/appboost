package com.app.ibooster;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.Log;

public class Utilities {
	
	public static String PREF_FIRST_LINE = "first_message";
	public static String PREF_SECOND_LINE = "second_message";
	public static String PREF_APP_ICON = "app_icon";

	public static String getIMEI(Context context) {
		String imei = null;
		try {
			TelephonyManager telephony = (TelephonyManager) context
					.getSystemService(Context.TELEPHONY_SERVICE);
			String imeinumber = telephony.getDeviceId();
			if (!isValidIMEI(imeinumber))
				imeinumber = Secure.getString(context.getContentResolver(),
						Secure.ANDROID_ID);
			MessageDigest mdEnc = MessageDigest.getInstance("MD5");

			mdEnc.update(imeinumber.getBytes(), 0, imeinumber.length());
			imei = asHex(mdEnc.digest());

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			Log.i(Booster.TAG, "Preference NSAE");
			Random rand = new Random();
			imei = "user" + rand.nextInt(Integer.MAX_VALUE);
		} catch (Throwable th) {
			th.printStackTrace();
			Log.e(Booster.TAG, "Problem fetching IMEI:" + th.getMessage());
		}
		return imei;
	}
	
	public static boolean isValidIMEI(String string) {
		try {
			long l = Long.valueOf(string);
			if (l != 0)
				return true;
			else
				return false;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	public static String asHex(byte[] buf) {
		char[] HEX_CHARS = "0123456789abcdef".toCharArray();
		char[] chars = new char[2 * buf.length];
		for (int i = 0; i < buf.length; ++i) {
			chars[2 * i] = HEX_CHARS[(buf[i] & 0xF0) >>> 4];
			chars[2 * i + 1] = HEX_CHARS[buf[i] & 0x0F];
		}
		return new String(chars);
	}
	
	public static boolean checkInternetConnection(Context context) {
		ConnectivityManager localConnectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if ((localConnectivityManager.getActiveNetworkInfo() != null)
				&& (localConnectivityManager.getActiveNetworkInfo()
						.isAvailable())
				&& (localConnectivityManager.getActiveNetworkInfo()
						.isConnected()))
			;
		return true;
	}

	public static void putMessage(Context context, String pref_name, String string) {
		if(string == null){
			string = "";
		}
			SharedPreferences prefs = PreferenceManager
					.getDefaultSharedPreferences(context);
			Editor editor = prefs.edit();
			editor.putString(pref_name, string);
			editor.commit();
	}

	public static String getMessage(Context context, String pref_name) {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		String message = prefs.getString(pref_name, null);
		return message;
	}
	
	public static int getDrawableByName(Context ctx, String name) {
		return ctx.getResources().getIdentifier(name, "drawable",
				ctx.getPackageName());
	}
	
}
