package com.app.ibooster;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;

public class Booster {

	private static long TIME_ONE_DAY = 24 * 60 * 60 * 1000;
	private static long DEBUG_TIME = 6000;
	public static String TAG = "booster";

	public static Context context;
	
	public static String DEFAULT_MESSAGE = " is waiting for you.";
	
	private static String REG_CLASS_NAME  = "registeredclass";
	
	public static void register(Context context, String classname){
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		Log.i(Booster.TAG, "setting classname : "+classname);
		prefs.edit().putString(REG_CLASS_NAME, classname).commit();
	}
	
	public static String getRegisterClass(Context context){
		String classname = PreferenceManager.getDefaultSharedPreferences(context).getString(REG_CLASS_NAME, null);
		Log.i(Booster.TAG, "setting classname : "+classname);
		return classname;
	}
	
	public static void startBoost(Context context){
		startBoost(context, null);
	}

	/**
	 * Start boost your app with this method.
	 * 
	 * @param context
	 * @param items
	 *            item[0] to show in BoostMessage's first line or null.
	 *            item[1] to show in BoostMessage's second line or null.
	 *            item[2] to show icon name, it will be used as message icon.
	 */
	public static void startBoost(Context context, String... items) {
		// TODO change debug time to real one day time.
		Log.i(TAG, "booster initialized.");
		Booster.context = context;
		String first_message = null;
		String second_message = null;
		String icon_name = null;
		
		if(items != null && items.length > 0){
			try {
				first_message = items[0];
				second_message = items[1];
				icon_name = items[2];
			} catch (Exception e) {
				Log.i(Booster.TAG, "item : "+first_message +" item2 : "+second_message);
				e.printStackTrace();
			}
		}
		
		Utilities.putMessage(context, Utilities.PREF_FIRST_LINE, first_message);
		Utilities.putMessage(context, Utilities.PREF_SECOND_LINE, second_message);
		Utilities.putMessage(context, Utilities.PREF_APP_ICON, icon_name);
		
		Long time = SystemClock.elapsedRealtime() + DEBUG_TIME;
		Intent intent = new Intent(context, BoosterReceiver.class);
		intent.setPackage(context.getPackageName());
		PendingIntent pintent = PendingIntent.getBroadcast(context, 0, intent,
				PendingIntent.FLAG_CANCEL_CURRENT);
		AlarmManager alarm = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		Log.i(Booster.TAG, "Setting alarm");
		alarm.set(AlarmManager.ELAPSED_REALTIME, time, pintent);
	}

	public static String verifyMessage(Context context, String pref_name, String message) {
		if(pref_name.equals(Utilities.PREF_FIRST_LINE)){
			try {
				if (message != null && !message.equals("")) {
					return message;
				}
				PackageManager pmanager = context.getPackageManager();
				String appname = pmanager.getApplicationLabel(context.getApplicationInfo()).toString();
				return appname;
			} catch (Exception e) {
				return "";
			}
		}
		else
			if(pref_name.equals(Utilities.PREF_SECOND_LINE)){
				try {
					if (message != null && !message.equals("")) {
						return message;
					}
					PackageManager pm = context.getPackageManager();
					ApplicationInfo ai = pm.getApplicationInfo(context.getPackageName(), 0);
					String name = (String)  (ai != null ? pm.getApplicationLabel(ai) : "");
					return name + DEFAULT_MESSAGE;
				} catch (Exception e) {
					return "";
				}
			}
			else
				if(pref_name.equals(Utilities.PREF_APP_ICON)){
					try{
						if(message == null){
							int icon = context.getApplicationInfo().icon;
							return icon+"";
						}
						else{
							try{
								int id = Utilities.getDrawableByName(context, message);
								return id+"";
							}
							catch(Exception e){
								Log.i(Booster.TAG, "APP_ICON id error");
								int icon = context.getApplicationInfo().icon;
								return icon+"";
							}
						}
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}
		return null;
	}

}
