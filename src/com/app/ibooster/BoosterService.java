package com.app.ibooster;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import org.json.JSONObject;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

public class BoosterService extends Service {
	
	private static final String IMPRESSION_URL = "http://23.21.65.231:8020/shown";
	private static final String CLICK_URL = "http://23.21.65.231:8020/clicked";
	
	private static final String KEY_IMEI = "imei";
	private static final String KEY_PACKAGE_NAME = "pakage_name";
	private static final String KEY_ALERT_TYPE = "notification_type";
	
	public static final String SDK_ACTION = "action";
	public static final String SHOW_TYPE_NOTI = "noti";
	public static final String REACH_TYPE_CLICK = "click";
	
	private static final int MAX_ATTEMPTS = 5;
	private static final int BACKOFF_MILLI_SECONDS = 2000;
	private static final Random random = new Random();

	@Override
	public void onCreate() {
		super.onCreate();
	}

	public int onStartCommand(final Intent intent, int paramInt1, int paramInt2) {
		new Thread(new Runnable(){
			@Override
			public void run() {
				sendData(intent);
			}
		}).start();
		return 1;
	}
	
	private static boolean sendDataToServer(Context context, String action) {
		Map<String, String> params = new HashMap<String, String>();
		params.put(KEY_IMEI, Utilities.getIMEI(context));
		params.put(KEY_PACKAGE_NAME, context.getPackageName());
		params.put(KEY_ALERT_TYPE, SHOW_TYPE_NOTI);
		
		long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
		for (int i = 1; i <= MAX_ATTEMPTS; i++) {
			Log.d(Booster.TAG, "Attempt #" + i + " to register");
			try {
				Log.i(Booster.TAG, "server_registering" + i + " of " + MAX_ATTEMPTS);
				String url = "";
				if(action.equals(SHOW_TYPE_NOTI)){
					url = IMPRESSION_URL;
				}
				else
					if(action.equals(REACH_TYPE_CLICK)){
						url = CLICK_URL;
					}
				if(get(context, url, params))
					return true;
			} catch (IOException e) {
				// Here we are simplifying and retrying on any error; in a real
				// application, it should retry only on unrecoverable errors
				// (like HTTP error code 503).
				Log.e(Booster.TAG, "Failed to register on attempt " + i, e);
				if (i == MAX_ATTEMPTS) {
					break;
				}
				try {
					Log.d(Booster.TAG, "Sleeping for " + backoff + " ms before retry");
					Thread.sleep(backoff);
				} catch (InterruptedException e1) {
					// Activity finished before we complete - exit.
					Log.d(Booster.TAG, "Thread interrupted: abort remaining retries!");
					Thread.currentThread().interrupt();
					return false;
				}
				// increase backoff exponentially
				backoff *= 2;
			}
			catch(Exception e){
				Log.d(Booster.TAG, "exception "+e.getMessage());
			}
		}
		Log.i(Booster.TAG, "server_register_error:" + MAX_ATTEMPTS);
		return false;
	}
	
	private static boolean post(Context context, String endpoint, Map<String, String> params) throws IOException {
		URL url = null;
		try {
			url = new URL(endpoint);
		} catch (MalformedURLException e) {
			Log.i(Booster.TAG, "invalid url: " + endpoint);
		}
		StringBuilder bodyBuilder = new StringBuilder();
		Iterator<Entry<String, String>> iterator = params.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, String> param = iterator.next();
			bodyBuilder.append(param.getKey()).append('=').append(param.getValue());
			if (iterator.hasNext()) {
				bodyBuilder.append('&');
			}
		}
		String body = bodyBuilder.toString();
		byte[] bytes = body.getBytes();
		HttpURLConnection conn = null;
		try {
			conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setFixedLengthStreamingMode(bytes.length);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
			// post the request
			OutputStream out = conn.getOutputStream();
			out.write(bytes);
			out.close();
			// handle the response
			int status = conn.getResponseCode();
			if (status != 200) {
				throw new IOException("Post failed with error code " + status);
			}
			
			InputStream in = conn.getInputStream();
			
			if (in != null) {
				StringBuilder builder = new StringBuilder();
				String line;
				try {
					BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
				} finally {
					in.close();
				}
				
				String response = builder.toString();
				
				// JSONObject jsonObject = new JSONObject(response);
				// boolean success = jsonObject.getBoolean("success");
				Log.i(Booster.TAG, "response :" + response);
				boolean success = response.equals("success");
				if (!success){
					return false;
				}
				else
					return true;
			}
		} catch (Throwable th) {
			Log.i(Booster.TAG, "Error in posting :" + th.getMessage());
			throw new IOException("Error in posting :" + th.getMessage());
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
		return false;
	}
	
	private static boolean get(Context context, String endpoint, Map<String, String> params) throws IOException {
		URL url = null;
		if(endpoint == null || endpoint.equals(""))
			return false;
		StringBuilder bodyBuilder = new StringBuilder();
		Iterator<Entry<String, String>> iterator = params.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, String> param = iterator.next();
			bodyBuilder.append(param.getKey()).append('=').append(param.getValue());
			if (iterator.hasNext()) {
				bodyBuilder.append('/');
			}
		}
		String tailurl = bodyBuilder.toString();
		if(!endpoint.endsWith("/"))
			endpoint = endpoint + "/";
		try {
			url = new URL(endpoint + tailurl);
		} catch (MalformedURLException e) {
			Log.i(Booster.TAG, "invalid url: " + endpoint);
		}
		
		HttpURLConnection conn = null;
		try {
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
			// handle the response
			int status = conn.getResponseCode();
			if (status != 200) {
				throw new IOException("Post failed with error code " + status);
			}
			
			InputStream in = conn.getInputStream();
			
			if (in != null) {
				StringBuilder builder = new StringBuilder();
				String line;
				try {
					BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
				} finally {
					in.close();
				}
				
				String response = builder.toString();
				
				JSONObject jsonObject = new JSONObject(response);
				Log.i(Booster.TAG,"response : "+jsonObject.toString());
				String success = jsonObject.getString("response");
				if (success.equals("success"))
					return true;
				else
					return false;
			}
		} catch (Throwable th) {
			Log.i(Booster.TAG, "Error in posting :" + th.getMessage());
			throw new IOException("Error in posting :" + th.getMessage());
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
		return false;
	}

	private void sendData(Intent intent) {
		Log.i(Booster.TAG, "Download Ticker doInBackground()");
		Context context = getApplicationContext();
		if (Utilities.checkInternetConnection(context)){
			if(intent != null){
				Bundle extra = intent.getExtras();
				if(extra != null){
					String action = extra.getString(SDK_ACTION);
					if (BoosterService.sendDataToServer(context, action)){
						stopSelf();
					}
				}
			}
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}	
}